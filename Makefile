H=@
ROOTD:=lua-deb-tools/
ROOT :=$(shell pwd)/$(ROOTD)
export ROOT 

help: test-installed
	$H echo
	$H echo 'available commands:'
	$H echo
	$H echo ' clean                  - clean all build-areas'
	$H echo ' changes P=srcpkg       - diffs last tag with trunk'
	$H echo ' torelease              - lists packages with pending changes'
	$H echo ' build P=srcpkg [V=ver] - default ver is svn head'
	$H echo ' buildall [I=install]   - build all packages installing them if I!=""'
	$H echo ' debdiff P=srcpkg       - debdiff with version in unstable'
	$H echo ' lintian [P=srcpkg]     - lintian the package(s)'
	$H echo ' graph                  - generates a png file with the deps'
	$H echo ' update                 - runs mr update with the right flags'
	$H echo
	$H echo Packages found:
	$H echo -n ' '
	$H . $(ROOT)scripts/common-functions.sh; \
	  build_order;\
	  echo $$RC

graph: test-installed
	$H if [ -e build-area/Packages -a -e build-area/Sources ]; then\
		echo "Using build-area/Packages and build-area/Sources";\
		$(ROOT)scripts/gv-deps.lua build \
			build-area/Packages build-area/Sources;\
		$(ROOT)scripts/gv-deps.lua run \
			build-area/Packages build-area/Sources;\
	else\
		echo "Using */debian/control, no virtual packages :-(";\
		echo "use make buildall to fix that";\
		$(ROOT)scripts/gv-deps.lua build */debian/control;\
		$(ROOT)scripts/gv-deps.lua run */debian/control;\
	fi

lintian: test-installed
	$H if [ -z $(P) ]; then\
		lintian -vi build-area/*.dsc;\
		lintian -vi build-area/*.changes;\
	else\
		lintian -vi build-area/$(P)*.dsc;\
		lintian -vi build-area/$(P).deb;\
	fi

debdiff: test-installed
	$H if [ -z $(P) ]; then echo please append P=src-pkg; exit 1; fi
	$H cd build-area;\
	for X in $(P)*.deb; do\
		pkg=`echo $$X | sed s/_.*//`;\
		mkdir diff;\
		cd diff;\
		echo downloading $$pkg;\
		aptitude download $$pkg/unstable >/dev/null;\
		debdiff $$pkg*deb ../$$X;\
		cd ..;\
		rm -rf diff;\
	done

build: test-installed
	$H if [ -z $(P) ]; then echo please append P=src-pkg; exit 1; fi
	$H . $(ROOT)scripts/common-functions.sh; \
	if [ -z $(V) ]; then version=master; else version=$(V); fi;\
	build $(P) $$version

buildall: test-installed
	$H . $(ROOT)scripts/common-functions.sh; \
	build_order;\
	DONE="";\
	FAIL="";\
	rm -rf build-area/*;\
	for X in $$RC; do\
		echo -n "building $$X: ";\
		build $$X master > build-area/$$X.build.log 2>&1;\
		if [ $$RC -eq 0 ]; then\
			if [ $$SKIP -eq 1 ]; then\
				echo SKIPPED;\
			else \
				echo OK;\
			fi;\
			DONE="$$DONE $$X";\
		else\
			FAIL="$$FAIL $$X";\
			echo FAIL;\
		fi;\
		if [ ! -z "$(I)" ]; then\
			sudo dpkg -i build-area/*.deb;\
		fi;\
	done;\
	echo FAIL=$$FAIL;\
	echo DONE=$$DONE;\
	dpkg-scanpackages build-area/ > build-area/Packages;\
	dpkg-scansources build-area/ > build-area/Sources;

torelease: test-installed
	$H . $(ROOT)scripts/common-functions.sh; \
	build_order; \
	for P in $$RC; do\
		last_tag $$P;\
		tag=$$RC;\
		if [ "$$tag" = "debian/" ]; then \
			echo "$$P was never uploaded/tagged";\
			continue;\
		fi;\
		DIFF=`cd $$P; git diff $$tag master | grep '^--- ' \
			| grep -v changelog | wc -l`;\
		if [ $$DIFF -ge 1 ]; then \
			echo "$$P has $$DIFF changed file(s) from $$tag";\
		fi;\
	done

changes: test-installed
	$H if [ -z $(P) ]; then echo please append P=src-pkg; exit 1; fi
	$H . $(ROOT)scripts/common-functions.sh; \
	last_tag $(P);\
	tag=$$RC;\
	cd $$P; git diff $$tag master 
	
clean: test-installed
	rm -rf build-area/*

update:
	mr -c mrconfig -j3 update

test-installed:
	$H B=$$(basename `pwd`);\
	  if [ "$$B" = "$(ROOTD)" ]; then\
	  	echo;\
	  	echo "This Makefile is supposed to be in ..";\
		echo "Please: ln -s $(ROOTD)/Makefile ../"; \
	  	echo;\
	  	exit 1;\
	  fi

