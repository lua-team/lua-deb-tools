---
-- Module to parse control files
--

local l = require "lpeg"
local select, assert, print, tconcat = select, assert, print, table.concat
local ipairs = ipairs

-- ============================ helpers ======================================

---
-- given a variable number of arguments builds a table containing them
-- @param ... items.
-- @return table containing the items, interesting in LPEG / expressions.
local function pack(...) return {...} end

local function packflat(...)
	local tmp, len = {}, select('#',...) 
	for i=1, len do
		for _,v in ipairs(select(i,...)) do
			tmp[#tmp+1]=v
		end
	end
	return tmp
end

---
-- Given a variable number of arguments (but an even number) returns a 
-- table mapping elements of index N to values of index N+1.
-- For example pair(k1,v1,k2,v2) --> { k1 = v1, k2 = v2 }.
-- @ returns table from odd indexes to even values, interesting in LPEG / expr.
local function pair(...)
	local len = select('#',...) 
	assert(len % 2 == 0,
		"Pair called on a list with an odd number of items:\n"..
			tconcat({...},"\n"))
	local i,t = 1,{}
	while i <= len do
		local k = select(i,...)
		local v = select(i+1,...)
		t[k]=v
		i=i+2
	end
	return t
end

local function concat(s)
	return function(...)
		return tconcat({...},s)
	end
end

--[[
-- given a variable number of names N, returns a function that takes 
-- the same amount of arguments (we call them values) and returns a 
-- table mapping names to values. First name goes with first variable.
-- @returns function interesting for a / LPEG expression.
local function name(...) 
	local names = {...}
	return function(...)
		local len, tmp = select('#',...), {}
		assert(len == #names, "Names and named number mismatch")
		for i,n in ipairs(names) do
			tmp[n] = select(i,...)
		end
		return tmp
	end
end
--]]

local mark = function(s, where)
	print(where)
	return where
end

-- ========================== PEG stuff ===================================

local eol = l.P("\n");
local eot = eol + -1;
local blank = l.S(" \t")
local blanks = blank^0
local garbage = eol + blank;

local name = l.C((l.R("AZ") + l.R("az") + l.R("09") + l.S("-"))^1)
local value = blanks * l.C((1-eol + eol * blank * -eol)^1) * blanks * eot
local entry = name * l.P(":") * (value + (blanks * eot/""))

local pkgname = l.C((l.S("-+.") + l.R("AZ") + l.R("az") + l.R("09"))^1)
local constr = l.P("==") + l.P(">>") + l.P("<<") + l.P(">=") + l.P("<=") 
local version = l.P("(") * blanks * l.C(constr) * blanks * l.C((1-(blank+l.P(")")))^1) * blanks * l.P(")") / concat(" ")
local sep = l.P(",")
local substvar = l.P("${") * (1 - l.P("}"))^1 * l.P("}")
local dep = blanks * (pkgname * blanks * version +
        (pkgname / function(c) return c,"" end) + 
	substvar / function(c) return c,"" end) * blanks

local avoid_stack_overflow = function (p,n) 
	return (p * (p^-(n-1)) / pack)^0
end

module("debian.control")

---
-- A control file paragraph
paragraph = entry^1 / pair

---
-- A complete control file (a list of paragraphs separated by blank lines)
control = paragraph / pack * avoid_stack_overflow(eol^1 * paragraph, 100) / packflat

---
-- 
deps = dep * (sep^-1 * dep)^0 / pair

-- eof
