--- 
-- apt-cache like module

local lpeg = require "lpeg"
local set = require "set"
local io = require "io"
local debian = {
	control = require "debian.control"
}

local pairs, ipairs, assert, select = pairs, ipairs, assert, select
local print, setmetatable, error = print, setmetatable, error
local table = table

local function parse_deps(f) 
	local tmp = lpeg.match(debian.control.deps,f or "") 
	if tmp == nil then return nil end
	local l = {}
	for k,_ in pairs(tmp) do
		l[#l+1] = k -- we drop versions
	end
	return l
end

local cache_mt = { __index = {
	provides = function(c, name)
		return set.new(c.src2bin[name])
	end,
	
	vprovides = function(c, name)
		local s = set.new {}
		for _, p in ipairs(c.src2bin[name]) do
			s = s + set.new(parse_deps(c.bin_packages[p]['Provides']))
		end
		return s
	end,

	bdepends = function(c, name)
		local rc, src
		src = c.src_packages[name] or c.src_packages[c.bin2src[name]]
		return
			set.new(parse_deps(src['Build-Depends'])) +
			set.new(parse_deps(src['Build-Depends-Indep']))
	end,

	depends = function(c, p)
		local s = set.new {}
		if c.bin_packages[p] then
			s = s + set.new(parse_deps(c.bin_packages[p].Depends))
		elseif c.src_packages[p] then
			for _, b in ipairs(c.src2bin[p]) do
				s = s + set.new(parse_deps(c.bin_packages[b].Depends))
			end
		else
			error('unknown ' ..(p or 'nil'))
		end
		return s
	end,

	rdepends = function(c, p)
		local acc = set.new {}
		local d = c:depends(p)
		acc = acc + d
		for x in pairs(d) do
			acc = acc + c:rdepends(x)
		end
		return acc
	end,

	rbdepends = function(c, name)
		local tmp = set.new {}
		for s in pairs(c:bdepends(name)) do
			tmp = tmp + set.new(s)
			tmp = tmp + c:rdepends(s)
		end
		return tmp
	end,

    -- isvirtual = function(c,n) return c.bin_packages[n] == nil end,

	isdummy = function(c,n)
		return c.bin_packages[n] and
			c.bin_packages[n]['Section'] == 'oldlibs' and
			c.bin_packages[n]['Description']:match('Transitional package') and
			c.bin_packages[n]['Priority'] == 'extra' and
			c.bin_packages[n]['Architecture'] == 'all'
	end,
}}

module("debian.cache")

function new(...)
	local tmp = {
		bin_packages = {},
		src_packages = {},
		bin2src = {},
		src2bin = {},
	}

	local function find_source(l)
		local s = nil
		for _,t in ipairs(l) do
			if t.Source ~= nil then
				if s == nil then
					s = t.Source 
				else
					error("more then one source package")
				end
			end
		end
		return s
	end

	for i=1,select('#',...) do
		local control_file = assert(io.open(select(i,...),"r"))
		local data = control_file:read('*all')
		local l = lpeg.match(debian.control.control, data)
		assert(l,"Error parsing: " .. select(i,...))
		for _, t in ipairs(l) do
			if t.Binary ~= nil or (t.Source ~= nil and t.Package == nil) then
				--src package
				local sname = t.Package or t.Source
				assert(tmp.src_packages[sname] == nil,
					"Source package "..sname.." defined twice")
				tmp.src_packages[sname] = t 
				tmp.src2bin[sname] = tmp.src2bin[sname] or {}
				-- print("Adding source pkg: "..t.Package)
			elseif t.Package ~= nil then
				assert(tmp.bin_packages[t.Package] == nil,
					"Binary package "..t.Package.." defined twice")
				-- if the same, no Source field
				local sname = t.Source or t.Package
				tmp.src2bin[sname] = tmp.src2bin[sname] or {}
				tmp.bin_packages[t.Package] = t 
				tmp.bin2src[t.Package] = sname
				for _,k in ipairs(parse_deps(t.Provides) or {}) do
					tmp.bin2src[k] = sname
				end
				tmp.src2bin[sname][#tmp.src2bin[sname]+1] = t.Package
				-- print("Adding binary pkg: "..t.Package)
				-- for k,v in pairs(t) do
				-- 	print("  "..k..": "..v)
				-- end
			else
				local stuff = {}
				for k,v in pairs(t) do stuff[#stuff+1]=k..":"..v end
				error("stanza not source nor binary:"..
					table.concat(stuff,"\n  "))
			end
		end
	end

	setmetatable(tmp, cache_mt)
	return tmp
end

-- vim:set tabstop=4:
