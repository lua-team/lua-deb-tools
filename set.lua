---
-- set module with + / - operations

module("set", package.seeall)

local set_mt = {}

new = function(t)
	if type(t) == "table" then
		local tmp = {}
		for k,v in ipairs(t) do
			tmp[v] = true
		end
		setmetatable(tmp,set_mt)
		return tmp
	elseif type(t) == "nil" then
		local tmp = {}
		setmetatable(tmp,set_mt)
		return tmp
	else
		local tmp = {[t] = true}
		setmetatable(tmp,set_mt)
		return tmp
	end
end

set_mt = {
	__add = function(s1,s2) 
		local tmp = new {}
		for k in pairs(s1) do tmp[k] = true end
		for k in pairs(s2) do tmp[k] = true end
		return tmp
	end,
	__sub = function(s1,s2)
		local tmp = new {}
		for k in pairs(s1) do if s2[k] then tmp[k] = true end end
		return tmp
	end,
	__div = function(s1,s2)
		local tmp = new {}
		for k in pairs(s1) do if not s2[k] then tmp[k] = true end end
		return tmp
	end,
	__tostring = function(t)
		function keys(t)
			local tmp = {}
			for k in pairs(t) do tmp[#tmp+1] = k end
			table.sort(tmp)
			return tmp
		end
		return table.concat(keys(t),", ")
	end,
	__index = function(t,k)
		if k == "size" then 
			return function()
				local n = 0
				for _ in pairs(t) do n=n+1 end
				return n
			end
		end
	end
}

-- vim:set tabstop=4:
