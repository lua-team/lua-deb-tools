#!/usr/bin/env lua5.2

package.path = package.path ..";lua-deb-tools/?.lua;lua-deb-tools/?"
--package.path = package.path .. ";tools.git/?.lua;tools.git/?"

set = require "set"
debian = { cache = require "debian.cache" }
gv = require "gv"

function is_old_naming(n)
	return  (string.match(n,"^liblua5%.1%-") and
		not (string.match(n,"^liblua5%..%-dev") or 
		     string.match(n,"^liblua5%..%-0")))
		or n == 'lua5.1-policy-dev'
end

function fill(g,c)
	local all = set.new {}
	for p in pairs(c.src_packages) do
		all = all + c:provides(p) + c:vprovides(p)
	end
	for p in pairs(c.src_packages) do
		local n = gv.node(g,p)
		gv.setv(n,"shape","plaintext")
		gv.setv(n, "fontsize", 8)
		local l = {
			'<TABLE BORDER="0" ',
			'CELLBORDER="1" ',
			'CELLSPACING="0">',
			'<TR><TD BGCOLOR="grey95">',
			p,
			"</TD></TR>"
		}
		for k in pairs(c:provides(p)) do
			if c:isdummy(k) then
				l[#l+1] = '<TR><TD BGCOLOR="grey80">'..k.."</TD></TR>"
			else
				l[#l+1] = "<TR><TD>"..k.."</TD></TR>"
			end
		end
		for k in pairs(c:vprovides(p)) do
			if c:isdummy(k) and false then
				l[#l+1] = '<TR><TD BGCOLOR="grey80">'..k.."</TD></TR>"
			else
				l[#l+1] = '<TR><TD BGCOLOR="lemonchiffon">'..k.."</TD></TR>"
			end
		end
		l[#l+1]="</TABLE>"
		gv.setv(n,"label","<"..table.concat(l)..">")
		if kind == 'build' then
			for d in pairs(c:bdepends(p) - all) do
				local edge = gv.edge(g,p,c.bin2src[d])
				gv.setv(edge, "label", d)
				gv.setv(edge, "fontsize", 8)
				if c:isdummy(d) or is_old_naming(d) then
					gv.setv(edge,"fontcolor","red")
				end
			end
		elseif kind == 'run' then
			for d in pairs(c:depends(p) - all) do
				local edge = gv.edge(g,p,c.bin2src[d])
				gv.setv(edge, "label", d)
				gv.setv(edge, "fontsize", 8)
				if c:isdummy(d) or is_old_naming(d) then
					gv.setv(edge,"fontcolor","red")
				end
			end
		end
	end
end

args = {...}
kind = args[1]
table.remove(args,1)
cache = debian.cache.new(unpack(args))

g = assert(gv.digraph("luadep"))
gv.setv(g,"overlap","scale")
fill(g,cache)
gv.layout(g,'dot')
gv.render(g,'png','/tmp/gv-'..kind..'-deps.png')
print('Generated /tmp/gv-'..kind..'-deps.png')

-- eof
