#!/usr/bin/env lua5.1

package.path = package.path .. ";lua-deb-tools/?.lua;lua-deb-tools/?"
--package.path = package.path .. ";tools.git/?.lua;tools.git/?"

require "toposort"
require "set"
require "debian.cache"

function keys(t)
	local tmp = {}
	for k,_ in pairs(t) do tmp[#tmp+1]=k end
	return tmp
end

cache = debian.cache.new(unpack(arg))

l = keys(cache.src_packages)

all_pkgs = set.new {}

for _, c in ipairs(l) do
	--print(c, 'provides', cache:provides(c))
	all_pkgs = all_pkgs + cache:provides(c)
end

l = toposort.toposort(l, 
	function(x) return cache:bdepends(x) - all_pkgs end,
	function(x) return cache:provides(x) end) 

for _,p in ipairs(l) do print(p) end

-- require "serialize"
-- print(serialize.serialize(cache))
