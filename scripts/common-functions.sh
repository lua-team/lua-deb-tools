print_infos() {
	dpkg -I $1 | egrep '^ (Package|Depends|Section|Architecture|Version):'
	dpkg -c $1 | grep -v '/$' | \
			sed 's|^\([rwxsStl-]*\) \([a-z_-]*/[a-z_]*\)[^.]*\./\(.*\)$|\1\t\2\t\3|'

}

last_tag() {
	local pkg=$1
	local beated=0
	local tag=

	cd $pkg
	tmp=`mktemp`
	git tag | grep ^debian | sed 's/^debian.//' > $tmp
	for candidate in `cat $tmp`; do
	  local cand="`echo $candidate | sed 's/_/~/'`"
	  beated=0
	  for opponent in `cat $tmp`; do
	    local opp="`echo $opponent | sed 's/_/~/'`"
	    if [ "$candidate" != "$opponent" ]; then
	      if dpkg --compare-versions "$opp" gt "$cand"; then
		beated=1
	      fi
	    fi
	  done
	  if [ $beated = 0 ]; then
	    tag=$candidate
	    break
	  fi
	done
	cd ..

	RC=debian/$tag
}

build() {
	local pkg=$1
	local version=$2
	
	if [ "$version" = "master" ]; then
		cd $pkg
		gbp buildpackage -us -uc -tc --git-ignore-new --git-export-dir=../build-area/
		RC=$?
	else
		cd $pkg
		git checkout -b tmp-build debian/$version
		gbp buildpackage -us -uc -tc --git-ignore-new --git-export-dir=../build-area/ \
			--git-debian-branch=tmp-build
		RC=$?
	fi
	SKIP=0
	if [ "$version" = "master" ]; then
		cd ..
	else
		git checkout master
		git branch -D tmp-build
		cd ..
	fi
}

build_order() {
	RC=`$ROOT/scripts/build-order.lua */debian/control`
}
