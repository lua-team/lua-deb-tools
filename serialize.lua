
local type, format, tostring, rep = type, string.format, tostring, string.rep
local pairs, print = pairs, print

local function serialize_aux(name,value,indent)
	function q(s)
		if type(s) == "number" then return tostring(s) else
		return format("%q",tostring(s)) end
	end
	function comma(i) 
		if i ~= 0 then 
			return "," 
		else 
			return "" 
		end 
	end
	function sq(i,s) 
		if i == 0 then 
			return s 
		else 
			return "["..q(s).."]" 
		end 
	end
	local pad = rep(" ",indent)
	if type(value) == "table" then
		local tmp = {}
		tmp[#tmp+1] = pad..sq(indent,name).." = {\n"
		for k,v in pairs(value) do
			tmp[#tmp+1] = serialize_aux(k,v,indent+2)
		end
		tmp[#tmp+1] = pad.."}"..comma(indent).."\n"
		return table.concat(tmp)
	else
		return pad..sq(indent,name).." = "..
			q(value)..comma(indent).."\n"
	end
end

module("serialize")

function serialize(val)
	return "local " .. serialize_aux("tmp", val, 0) .. "\nreturn tmp"
end
