---
-- topological sorting algorithm

local set = require "set"
local ipairs,print = ipairs,print

module("toposort")

local function split(l,p)
	local tmp_true, tmp_false = {}, {}
	for _,x in ipairs(l) do
		if p(x) then
			tmp_true[#tmp_true+1] = x
		else
			tmp_false[#tmp_false+1] = x
		end
	end
	return tmp_true, tmp_false
end

---
-- topologically sorts l, where element x of l depends ofer
-- deps_of(x) that must be a set and x provides a set
-- of elements outside l but on which an y in l may depend
function toposort(l,deps_of,provides)
	local processed, doable = set.new {}, {}
	local function no_deps_left(x)
		return (deps_of(x) / processed):size() == 0
	end
	local rc = {}
	while true do
		doable, l = split(l,no_deps_left)
		if #doable == 0 then break end
		for _,x in ipairs(doable) do
			rc[#rc+1] = x
			processed = processed + provides(x)
		end
	end
	return rc, l
end

